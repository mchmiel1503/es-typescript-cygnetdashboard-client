namespace es.api.mocks {

    export var EXAMPLE_DASHBOARD = `
    [  
   {  
      "name":"Server Health",
      "rows":[  
         {  
            "header":"Hard Drive Space",
            "rows":[  
               {  
                  "label":"Total",
                  "score":3,
                  "order":2
               },
               {  
                  "label":"% Free ",
                  "score":3,
                  "order":3
               }
            ]
         },
         {  
            "label":"Ram % Free",
            "score":3,
            "order":4
         },
         {  
            "label":"CPU Usage (%)",
            "score":3,
            "order":5
         },
         {  
            "label":"Network Availability",
            "score":3,
            "order":6
         },
         {  
            "header":"Client Connections",
            "rows":[  
               {  
                  "label":"Authorized",
                  "score":3,
                  "order":7
               },
               {  
                  "label":"In Use",
                  "score":0,
                  "order":8
               }
            ]
         },
         {  
            "label":"Machine Running Time",
            "score":0,
            "order":9
         },
         {  
            "label":"Site Activation",
            "score":3,
            "order":10
         },
         {  
            "label":"Site Status",
            "score":3,
            "order":11
         },
         {  
            "header":"Service Run Time",
            "rows":[  
               {  
                  "label":"Max",
                  "score":3,
                  "order":12
               },
               {  
                  "label":"Min",
                  "score":3,
                  "order":13
               }
            ]
         },
         {  
            "header":"Disc Queue",
            "rows":[  
               {  
                  "label":"C:",
                  "score":0,
                  "order":14
               },
               {  
                  "label":"D:",
                  "score":3,
                  "order":15
               }
            ]
         },
         {  
            "label":"VHS % Full",
            "score":3,
            "order":16
         },
         {  
            "header":"UIS Quere",
            "rows":[  
               {  
                  "label":"Comm Queue",
                  "score":1,
                  "order":17
               }
            ]
         },
         {  
            "label":"Processing Queue",
            "score":3,
            "order":18
         },
         {  
            "header":"GNS Queue",
            "rows":[  
               {  
                  "label":"Notifications",
                  "score":2,
                  "order":19
               },
               {  
                  "label":"Resend",
                  "score":3,
                  "order":20
               }
            ]
         }
      ]
   }
    ]`

}